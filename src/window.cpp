/*
 * Copryright (C) 2019 Victor A. Santos <victoraur.santos@gmail.com>
 *
 * This file is part of Matex Compositor.
 *
 * Matex Compositor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Matex Compositor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Matex Compositor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "window.h"

#include "compositor.h"
#include "view.h"

#include <QMouseEvent>
#include <QOpenGLFunctions>
#include <QWaylandSeat>

namespace Matex
{
Window::Window() : m_comp(nullptr) {}

void Window::setCompositor(Compositor *comp) { this->m_comp = comp; }

void Window::initializeGL()
{
  QImage bg(800, 600, QImage::Format_RGBA8888);
  bg.fill(Qt::blue);

  m_backgroundTexture = new QOpenGLTexture(bg, QOpenGLTexture::DontGenerateMipMaps);
  m_backgroundTexture->setMinificationFilter(QOpenGLTexture::Nearest);
  m_textureBlitter.create();

  if (m_comp)
    m_comp->create();
}

void Window::drawBackground()
{
  for (int y = 0; y < height(); y += 600)
  {
    for (int x = 0; x < width(); x += 800)
    {
      QMatrix4x4 targetTransform =
          QOpenGLTextureBlitter::targetTransform(QRect(QPoint(x, y), QSize(800, 600)), QRect(QPoint(0, 0), size()));
      m_textureBlitter.blit(m_backgroundTexture->textureId(), targetTransform, QOpenGLTextureBlitter::OriginTopLeft);
    }
  }
}

View *Window::viewAt(const QPointF &point)
{
  View *ret = nullptr;

  Q_FOREACH (View *view, m_comp->views())
  {
    // if (view == m_dragIconView)
    //  continue;
    QRectF geom(view->position(), view->size());
    if (geom.contains(point))
      ret = view;
  }

  return ret;
}

void Window::paintGL()
{
  m_comp->startRender();

  QOpenGLFunctions *functions = context()->functions();

  functions->glClearColor(1.f, .6f, .0f, 0.5f);
  functions->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  m_textureBlitter.bind();
  drawBackground();

  functions->glEnable(GL_BLEND);
  functions->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  Q_FOREACH (View *view, m_comp->views())
  {
    auto texture = view->getTexture();
    if (!texture)
      continue;
    QWaylandSurface *surface = view->surface();
    if ((surface && surface->hasContent()) || view->isBufferLocked())
    {
      QSize s = view->size();
      if (!s.isEmpty())
      {
        QRectF surfaceGeometry(view->position(), s);
        auto surfaceOrigin = view->textureOrigin();
        QRectF targetRect(surfaceGeometry.topLeft(), surfaceGeometry.size());
        QMatrix4x4 targetTransform = QOpenGLTextureBlitter::targetTransform(targetRect, QRect(QPoint(), size()));
        m_textureBlitter.blit(texture->textureId(), targetTransform, surfaceOrigin);
      }
    }
  }
  functions->glDisable(GL_BLEND);

  m_textureBlitter.release();
  m_comp->endRender();
}

void Window::mousePressEvent(QMouseEvent *me)
{
  if (m_mouseView.isNull())
  {
    m_mouseView = viewAt(me->localPos());
    if (!m_mouseView)
    {
      // TODO: m_comp->closePopups();
      return;
    }
    if (me->modifiers() == Qt::AltModifier || me->modifiers() == Qt::MetaModifier)
      m_grabState = MoveGrab; // start move
    else
      m_comp->raise(m_mouseView);
    m_initialMousePos = me->localPos();
    m_mouseOffset = me->localPos() - m_mouseView->position();

    sendMouseEvent(me, m_mouseView);
  }

  sendMouseEvent(me, m_mouseView);
}

void Window::mouseReleaseEvent(QMouseEvent *me)
{
  if (!mouseGrab())
    sendMouseEvent(me, m_mouseView);
  if (me->buttons() == Qt::NoButton)
  {
    if (m_grabState == DragGrab)
    {
      View *view = viewAt(me->localPos());
      m_comp->handleDrag(view, me);
    }
    m_mouseView = nullptr;
    m_grabState = NoGrab;
  }
}

void Window::mouseMoveEvent(QMouseEvent *me)
{
  switch (m_grabState)
  {
  case NoGrab:
  {
    View *view = m_mouseView ? m_mouseView.data() : viewAt(me->localPos());
    sendMouseEvent(me, view);
    if (!view)
      setCursor(Qt::ArrowCursor);
  }
  break;
  case MoveGrab:
  {
    m_mouseView->setPosition(me->localPos() - m_mouseOffset);
    update();
  }
  break;
  case ResizeGrab:
  {
    QPoint delta = (me->localPos() - m_initialMousePos).toPoint();
    m_comp->handleResize(m_mouseView, m_initialSize, delta, m_resizeEdge);
  }
  break;
  case DragGrab:
  {
    View *view = viewAt(me->localPos());
    m_comp->handleDrag(view, me);

    if (m_dragIconView)
    {
      m_dragIconView->setPosition(me->localPos() + m_dragIconView->offset());
      update();
    }
  }
  break;
  }
}

void Window::keyPressEvent(QKeyEvent *e) { m_comp->defaultSeat()->sendKeyPressEvent(e->nativeScanCode()); }

void Window::keyReleaseEvent(QKeyEvent *e) { m_comp->defaultSeat()->sendKeyReleaseEvent(e->nativeScanCode()); }

bool Window::mouseGrab() const { return m_grabState != NoGrab; }

void Window::sendMouseEvent(QMouseEvent *me, View *target)
{
  QPointF mappedPos = me->localPos();

  if (target)
  {
    mappedPos -= target->position();
  }

  QMouseEvent viewEvent(me->type(), mappedPos, me->localPos(), me->button(), me->buttons(), me->modifiers());
  m_comp->handleMouseEvent(target, &viewEvent);
}
} // namespace Matex
