/*
 * Copryright (C) 2019 Victor A. Santos <victoraur.santos@gmail.com>
 *
 * This file is part of Matex Compositor.
 *
 * Matex Compositor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Matex Compositor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Matex Compositor.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QOpenGLTexture>
#include <QOpenGLTextureBlitter>
#include <QOpenGLWindow>
#include <QPointer>

namespace Matex
{
class Compositor;
class View;

class Window : public QOpenGLWindow
{
  Q_OBJECT

public:
  Window();

  void setCompositor(Compositor *comp);

protected:
  void initializeGL() override;
  void paintGL() override;
  void mousePressEvent(QMouseEvent *me) override;
  void mouseReleaseEvent(QMouseEvent *me) override;
  void mouseMoveEvent(QMouseEvent *me) override;
  void keyPressEvent(QKeyEvent *e) override;
  void keyReleaseEvent(QKeyEvent *e) override;

private:
  enum GrabState
  {
    NoGrab,
    MoveGrab,
    ResizeGrab,
    DragGrab
  };

  void drawBackground();
  View *viewAt(const QPointF &point);
  bool mouseGrab() const;
  void sendMouseEvent(QMouseEvent *me, View *target);

  Compositor *m_comp;
  QOpenGLTexture *m_backgroundTexture;
  QOpenGLTextureBlitter m_textureBlitter;
  QPointer<View> m_mouseView;
  QPointF m_mouseOffset;
  QPointF m_initialMousePos;
  GrabState m_grabState = NoGrab;
  QSize m_initialSize;
  Qt::Edges m_resizeEdge;
  View *m_dragIconView = nullptr;
};
} // namespace Matex
