/*
 * Copryright (C) 2019 Victor A. Santos <victoraur.santos@gmail.com>
 *
 * This file is part of Matex Compositor.
 *
 * Matex Compositor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Matex Compositor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Matex Compositor.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QOpenGLTextureBlitter>
#include <QWaylandView>
#include <QWaylandXdgSurfaceV6>

namespace Matex
{
class Compositor;

class View : public QWaylandView
{
  Q_OBJECT

public:
  View(Compositor *comp);

  void setParent(View *view);
  View *parent();
  QOpenGLTexture *getTexture();
  QOpenGLTextureBlitter::Origin textureOrigin() const;
  QSize size() const;
  QPointF position() const;
  void setPosition(const QPointF &pos);
  QPoint offset();

public slots:
  void onXdgSetMinimized();
  void onXdgSetMaximized();
  void onXdgUnsetMaximized();
  void onXdgSetFullscreen(QWaylandOutput *output);
  void onXdgUnsetFullscreen();
  void onOffsetForNextFrame(const QPoint &offset);

private:
  friend class Compositor;
  Compositor *m_comp = nullptr;
  View *m_parent = nullptr;
  QWaylandXdgSurfaceV6 *m_xdgSurface;
  QOpenGLTexture *m_texture = nullptr;
  QOpenGLTextureBlitter::Origin m_origin;
  QPointF m_position;
  QSize m_size;
  QPoint m_offset;
};
} // namespace Matex
