/*
 * Copryright (C) 2019 Victor A. Santos <victoraur.santos@gmail.com>
 *
 * This file is part of Matex Compositor.
 *
 * Matex Compositor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Matex Compositor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Matex Compositor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "view.h"

#include <QWaylandOutput>

namespace Matex
{
View::View(Compositor *comp) : m_comp(comp) {}

void View::setParent(View *view) { m_parent = view; }

View *View::parent() { return m_parent; }

QOpenGLTexture *View::getTexture()
{
  bool newContent = advance();
  QWaylandBufferRef buf = currentBuffer();
  if (!buf.hasContent())
    m_texture = nullptr;
  if (newContent)
  {
    m_texture = buf.toOpenGLTexture();
    if (surface())
    {
      m_size = surface()->size();
      m_origin = buf.origin() == QWaylandSurface::OriginTopLeft ? QOpenGLTextureBlitter::OriginTopLeft
                                                                : QOpenGLTextureBlitter::OriginBottomLeft;
    }
  }

  return m_texture;
}

QOpenGLTextureBlitter::Origin View::textureOrigin() const { return m_origin; }

QSize View::size() const { return surface() ? surface()->size() : m_size; }

QPointF View::position() const { return m_position; }

void View::setPosition(const QPointF &pos) { m_position = pos; }

QPoint View::offset() { return m_offset; }

void View::onXdgSetMinimized() { qDebug() << "Minimizing not supported yet!"; }

void View::onXdgSetMaximized()
{
  // we assume it is a toplevel
  m_xdgSurface->toplevel()->sendMaximized(output()->geometry().size());

  // An improvement here, would have been to wait for the commit after the ack_configure for the
  // request above before moving the window. This would have prevented the window from being
  // moved until the contents of the window had actually updated. This improvement is left as an
  // exercise for the reader.
  setPosition(QPoint(0, 0));
}

void View::onXdgUnsetMaximized() { m_xdgSurface->toplevel()->sendUnmaximized(); }

void View::onXdgSetFullscreen(QWaylandOutput *clientPreferredOutput)
{
  // should clients choose a output?
  QWaylandOutput *outputToFullscreen = clientPreferredOutput ? clientPreferredOutput : output();

  m_xdgSurface->toplevel()->sendFullscreen(outputToFullscreen->geometry().size());

  // An improvement here, would have been to wait for the commit after the ack_configure for the
  // request above before moving the window. This would have prevented the window from being
  // moved until the contents of the window had actually updated. This improvement is left as an
  // exercise for the reader.
  setPosition(outputToFullscreen->position());
}

void View::onXdgUnsetFullscreen() { onXdgUnsetMaximized(); }

void View::onOffsetForNextFrame(const QPoint &offset)
{
  m_offset = offset;
  setPosition(position() + offset);
}
} // namespace Matex
