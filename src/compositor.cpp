/*
 * Copryright (C) 2019 Victor A. Santos <victoraur.santos@gmail.com>
 *
 * This file is part of Matex Compositor.
 *
 * Matex Compositor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Matex Compositor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Matex Compositor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "compositor.h"

#include "view.h"

#include <QMouseEvent>
#include <QWaylandDrag>
#include <QWaylandSeat>

namespace Matex
{
Compositor::Compositor(QWindow *win) : m_win(win), m_xdgShell(new QWaylandXdgShellV6(this)) {}

void Compositor::create()
{
  QWaylandCompositor::create();
  auto *output = new QWaylandOutput(this, m_win);
  QWaylandOutputMode mode(QWaylandOutputMode(QSize(800, 600), 60000));
  output->addMode(mode);
  output->setCurrentMode(mode);

  connect(this, &QWaylandCompositor::surfaceCreated, this, &Compositor::onSurfaceCreated);
  connect(m_xdgShell, &QWaylandXdgShellV6::xdgSurfaceCreated, this, &Compositor::onXdgSurfaceCreated);
}

void Compositor::startRender()
{
  if (defaultOutput())
    defaultOutput()->frameStarted();
}

void Compositor::endRender()
{
  if (defaultOutput())
    defaultOutput()->sendFrameCallbacks();
}

QList<View *> Compositor::views() { return m_views; }

View *Compositor::findView(const QWaylandSurface *s) const
{
  Q_FOREACH (View *view, m_views)
  {
    if (view->surface() == s)
      return view;
  }
  return nullptr;
}

void Compositor::raise(View *view)
{
  int startPos = m_views.indexOf(view);
  int endPos = findEndOfChildTree(m_views, startPos);

  int n = m_views.count();
  int tail = n - endPos - 1;

  // bubble sort: move the child tree to the end of the list
  for (int i = 0; i < tail; i++)
  {
    int source = endPos + 1 + i;
    int dest = startPos + i;
    for (int j = source; j > dest; j--)
      m_views.swap(j, j - 1);
  }
}

void Compositor::triggerRender() { m_win->requestUpdate(); }

void Compositor::handleMouseEvent(QWaylandView *target, QMouseEvent *me)
{
  //  auto popClient = popupClient();

  //  if (target && me->type() == QEvent::MouseButtonPress && popClient && popClient != target->surface()->client()) {
  //    closePopups();
  //  }

  auto *seat = defaultSeat();
  auto *surface = target ? target->surface() : nullptr;

  switch (me->type())
  {
  case QEvent::MouseButtonPress:
    seat->sendMousePressEvent(me->button());

    if (surface == nullptr || surface->role() == QWaylandXdgPopupV6::role() ||
        surface->role() == QWaylandXdgToplevelV6::role())
    {
      if (surface != seat->keyboardFocus())
      {
        seat->setKeyboardFocus(surface);
      }
      if (target != seat->mouseFocus())
      {
        seat->setMouseFocus(target);
      }
    }
    break;
  case QEvent::MouseButtonRelease:
    seat->sendMouseReleaseEvent(me->button());
    break;
  case QEvent::MouseMove:
    seat->sendMouseMoveEvent(target, me->localPos(), me->globalPos());
    break;
  default:
    break;
  }
}

void Compositor::handleResize(View *target, const QSize &initialSize, const QPoint &delta, Qt::Edges edge)
{
  auto *xdgSurface = target->m_xdgSurface;

  if (xdgSurface)
  {
    QSize newSize = xdgSurface->toplevel()->sizeForResize(initialSize, delta, edge);
    xdgSurface->toplevel()->sendResizing(newSize);
  }
}

void Compositor::handleDrag(View *target, QMouseEvent *me)
{
  auto pos = me->localPos();
  QWaylandSurface *surface = nullptr;

  if (target)
  {
    pos -= target->position();
    surface = target->surface();
  }

  auto *currentDrag = defaultSeat()->drag();
  currentDrag->dragMove(surface, pos);

  if (me->buttons() == Qt::NoButton)
  {
    m_views.removeOne(findView(currentDrag->icon()));
    currentDrag->drop();
  }
}

void Compositor::onSurfaceCreated(QWaylandSurface *surface)
{
  connect(surface, &QWaylandSurface::surfaceDestroyed, this, &Compositor::onSurfaceDestroyed);
  connect(surface, &QWaylandSurface::hasContentChanged, this, &Compositor::onSurfaceHasContentChanged);
  connect(surface, &QWaylandSurface::redraw, this, &Compositor::triggerRender);
  connect(surface, &QWaylandSurface::subsurfacePositionChanged, this, &Compositor::onSubsurfacePositionChanged);

  View *view = new View(this);
  view->setSurface(surface);
  view->setOutput(outputFor(m_win));
  m_views << view;

  connect(view, &QWaylandView::surfaceDestroyed, this, &Compositor::onViewSurfaceDestroyed);
  connect(surface, &QWaylandSurface::offsetForNextFrame, view, &View::onOffsetForNextFrame);
}

void Compositor::onSurfaceDestroyed() { triggerRender(); }

void Compositor::onSurfaceHasContentChanged()
{
  auto *surface = qobject_cast<QWaylandSurface *>(sender());

  if (surface->hasContent())
  {
    if (surface->role() == QWaylandXdgToplevelV6::role() || surface->role() == QWaylandXdgPopupV6::role())
    {
      defaultSeat()->setKeyboardFocus(surface);
    }
  }

  triggerRender();
}

void Compositor::onSubsurfacePositionChanged(const QPoint &position)
{
  auto *surface = qobject_cast<QWaylandSurface *>(sender());

  if (!surface)
    return;

  View *view = findView(surface);
  view->setPosition(position);
  triggerRender();
}

void Compositor::onViewSurfaceDestroyed()
{
  auto *view = qobject_cast<View *>(sender());

  view->setBufferLocked(true);
  // TODO: view->startAnimation(false);
  // connect(view, &View::animationDone, this, &Compositor::viewAnimationDone);
}

void Compositor::onXdgSurfaceCreated(QWaylandXdgSurfaceV6 *surface)
{
  connect(surface, &QWaylandXdgSurfaceV6::toplevelCreated, this, &Compositor::onXdgToplevelCreated);
}

void Compositor::onXdgToplevelCreated()
{
  auto *surface = static_cast<QWaylandXdgSurfaceV6 *>(sender());
  auto toplevel = surface->toplevel();
  auto *view = findView(surface->surface());
  Q_ASSERT(view);

  connect(toplevel, &QWaylandXdgToplevelV6::startMove, this, &Compositor::onXdgStartMove);
  connect(toplevel, &QWaylandXdgToplevelV6::startResize, this, &Compositor::onXdgStartResize);

  connect(toplevel, &QWaylandXdgToplevelV6::setMinimized, view, &View::onXdgSetMinimized);
  connect(toplevel, &QWaylandXdgToplevelV6::setMaximized, view, &View::onXdgSetMaximized);
  connect(toplevel, &QWaylandXdgToplevelV6::setFullscreen, view, &View::onXdgSetFullscreen);
  connect(toplevel, &QWaylandXdgToplevelV6::unsetMaximized, view, &View::onXdgUnsetMaximized);
  connect(toplevel, &QWaylandXdgToplevelV6::unsetFullscreen, view, &View::onXdgUnsetFullscreen);

  view->m_xdgSurface = surface;

  //  view->startAnimation(true);
  view->setPosition(QPointF(100, 100));
}

void Compositor::onXdgStartMove()
{
  // TODO: close all popups
  emit startMove();
}

void Compositor::onXdgStartResize(QWaylandSeat *seat, Qt::Edges edges)
{
  Q_UNUSED(seat);
  emit startResize(edges, true);
}

int findEndOfChildTree(const QList<View *> &list, int index)
{
  int n = list.count();
  View *parent = list.at(index);

  while (index + 1 < n)
  {
    if (list.at(index + 1)->parent() != parent)
      break;
    index = findEndOfChildTree(list, index + 1);
  }

  return index;
}
} // namespace Matex
