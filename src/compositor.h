/*
 * Copryright (C) 2019 Victor A. Santos <victoraur.santos@gmail.com>
 *
 * This file is part of Matex Compositor.
 *
 * Matex Compositor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Matex Compositor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Matex Compositor.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QWaylandCompositor>
#include <QWaylandXdgShellV6>
#include <QWindow>

namespace Matex
{
class View;

class Compositor : public QWaylandCompositor
{
  Q_OBJECT

public:
  Compositor(QWindow *win);

  void create() override;
  void startRender();
  void endRender();
  QList<View *> views();
  void raise(View *view);
  void handleMouseEvent(QWaylandView *target, QMouseEvent *me);
  void handleResize(View *target, const QSize &initialSize, const QPoint &delta, Qt::Edges edge);
  void handleDrag(View *target, QMouseEvent *me);

signals:
  void startMove();
  void startResize(Qt::Edges edges, bool anchored);

public slots:
  void triggerRender();
  void onSurfaceCreated(QWaylandSurface *surface);
  void onSurfaceDestroyed();
  void onSurfaceHasContentChanged();
  void onSubsurfacePositionChanged(const QPoint &position);
  void onViewSurfaceDestroyed();

  /* XDG Shell specific slots */
  void onXdgSurfaceCreated(QWaylandXdgSurfaceV6 *surface);
  void onXdgToplevelCreated();
  void onXdgStartMove();
  void onXdgStartResize(QWaylandSeat *seat, Qt::Edges edges);

private:
  View *findView(const QWaylandSurface *surface) const;

  QWindow *m_win;
  QWaylandXdgShellV6 *m_xdgShell;
  QList<View *> m_views;
};

int findEndOfChildTree(const QList<View *> &list, int index);
} // namespace Matex
