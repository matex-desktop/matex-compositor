/*
 * Copryright (C) 2019 Victor A. Santos <victoraur.santos@gmail.com>
 *
 * This file is part of Matex Compositor.
 *
 * Matex Compositor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Matex Compositor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Matex Compositor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "compositor.h"

#include "config.h"
#include "window.h"

#include <QApplication>
#include <QCommandLineParser>

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
  QCoreApplication::setApplicationName("matex-compositor");
  QCoreApplication::setApplicationVersion(VERSION);

  QCommandLineParser parser;
  parser.setApplicationDescription("The Matex's Compositor");
  parser.addHelpOption();
  parser.addVersionOption();

  parser.addOptions({{"nested-window", "Run compositor nested on a window"}});

  parser.process(app);

  // this is causing a crash
  // if (parser.isSet("nested-window"))
  // {
  setenv("QT_XCB_GL_INTEGRATION", "xcb_egl", true);
  setenv("QT_WAYLAND_CLIENT_BUFFER_INTEGRATION", "xcomposite-egl", true);

  Matex::Window window;
  Matex::Compositor comp(&window);
  window.setCompositor(&comp);

  window.resize(800, 600);
  window.show();
  //}

  return app.exec();
}
